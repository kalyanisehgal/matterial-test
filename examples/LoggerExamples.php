<?php
require '../vendor/autoload.php';

use Matterial\Matterial;
use Monolog\Logger;
use Monolog\Handler\StreamHandler as MonologStreamHandler;

function logging(){
    $client = new Matterial\Client(array('log_path' => 'c:/wamp64/www/logs/matterial.logs'));
    $client->setLoggerPath('c:/wamp64/www/logs/matterial.logs');
    $logger =  $client->getLogger();
    $logger->info("Woopie! My Test Logging Worked.");
}