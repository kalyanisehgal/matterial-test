<?php
require '../vendor/autoload.php';

use Matterial\Matterial;
use Monolog\Logger;
use Monolog\Handler\StreamHandler as MonologStreamHandler;

//Login Functionality
function login() {
    try {
        $client = new Matterial\Client(
            array(
                'login' => 'kalyani+matterial@homepageit.net',
                'password' => 'homepageit@123'
            )
        );
    
        if($client->createSession(Matterial\Constants::LOGON)){
            echo "Login Worked";
        } else {
            echo "Some Error Occured";
        }  
    }
    
    catch (Matterial\MattException $e) {
        echo $e->errorMessage();
    }
    
    catch(Exception $e) {
        echo $e->getMessage();
    }    
}

//PreLogin Functionality
function preLogin() {
    try {
        $client = new Matterial\Client(
            array(
                'login' => 'kalyani+matterial@homepageit.net',
                'password' => 'homepageit@123'
            )
        );
        
        $basePath = $client->createSession(Matterial\Constants::PRELOGON);

        if($basePath){
            echo "API BASE PATH: ".$basePath;
        } else {
            echo "Some Error Occured";
        } 
    }
    
    catch (Matterial\MattException $e) {
        echo $e->errorMessage();
    }
    
    catch(Exception $e) {
        echo $e->getMessage();
    }    
}

//PreChangeInstance
function preChangeInstance() {
    try {
        $client = new Matterial\Client(
            array(
                'login' => 'kalyani+matterial@homepageit.net',
                'password' => 'homepageit@123'
            )
        );
    
        if($client->createSession(Matterial\Constants::LOGON)){
            $result = $client->api(Matterial\Constants::AUTHENTICATE)->preChangeInstance();

            echo "<pre>";
            print_r($result);
            echo "</pre>";
        } else {
            echo "Some Error Occured";
        }  
    }
    
    catch (Matterial\MattException $e) {
        echo $e->errorMessage();
    }
    
    catch(Exception $e) {
        echo $e->getMessage();
    }    
}



echo "<h3>LOGIN</h3>";
//login();

echo "<h3>PRELOGIN</h3>";
preLogin();

echo "<h3>PRECHANGEINSTANCE</h3>";
//preChangeInstance();




