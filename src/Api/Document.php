<?php

namespace Matterial\Matterial\Api;

/**
 * Document Api Wrapper
 *
 * @link   https://www.matterial.com/en/documentation/api/311#document
 * @author Matterial.com
 */
class Document extends AbstractApi
{
    protected $documents;
    /**
     * Get duplicate of document by id
     *
     * @return JSON
     */
    public function getDuplicateDocById( $documentId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>",$documentId,\Matterial\Matterial\ApiConstants::DUPLICATEDOCBYID);
        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Get duplicates
     *
     * @return JSON
     */
    public function getDuplicates()
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $this->documents = $this->get(\Matterial\Matterial\ApiConstants::DUPLICATES,$header);

        return $this->documents;
    }

    /**
     * Remove duplicate
     *
     * @return 
     */
    public function removeDuplicate( $contextToken = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<contextToken>",$contextToken,\Matterial\Matterial\ApiConstants::DUPLICATEBYCONTEXT);

        $this->documents = $this->delete( $api, $header );

        return $this->documents;
    }

    /**
     * Get document(s) by ID
     *
     * @return 
     */
    public function getDocuments( array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );


        $api = \Matterial\Matterial\ApiConstants::DOCUMENT;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            echo $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Get document(s) in trash
     *
     * @return 
     */
    public function getTrashDocuments( array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );


        $api = \Matterial\Matterial\ApiConstants::GETTRASHDOCUMENTS;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Add document(s) to session clipboard
     *
     * @return integer -number of document added to clipboard
     */
    public function putDocumentsToClipboard( array $data = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->put(\Matterial\Matterial\ApiConstants::ADDCBDOCUMENTS, $data, $header );

        return $this->documents;
    }

    /**
     * Remove document(s) from session clipboard
     *
     * @return Integer - number of documents removed from clipboard
     */
    public function removeDocumentsFromClipboard( array $data = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->put(\Matterial\Matterial\ApiConstants::REMOVECBDOCUMENTS, $data, $header );

        return $this->documents;
    }

    /**
     * Clear all documents from session clipboard.
     *
     * @return Integer - number of documents removed from clipboard
     */
    public function clearDocumentsFromClipboard()
    {
        $header = array(
            'content-type' => 1,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->delete(\Matterial\Matterial\ApiConstants::CLEARCBDOCUMENTS, $header );

        return $this->documents;
    }

    /**
     * Create Snap.
     *
     * @return JSON - document object
     */
    public function createSnap( array $data = array() , array $query = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::CREATESNAP;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->post( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Create Template.
     *
     * @return JSON - document object
     */
    public function createTemplate( array $data = array() , array $query = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::TEMPLATE;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->post( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Update Template.
     *
     * @return JSON - document object
     */
    public function updateTemplate( array $data = array() , array $query = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::TEMPLATE;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'/'.$data['id'].'?'.$params;
        }

        $this->documents = $this->put( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Update Bio.
     *
     * @return JSON - document object
     */
    public function updateBio( array $data = array() , array $query = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::UPDATEBIO;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'/'.$data['id'].'?'.$params;
        }

        $this->documents = $this->put( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Create Documet.
     *
     * @return JSON - document object
     */
    public function createDocument( array $data = array() , array $query = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::DOCUMENT;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->post( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Update Document.
     *
     * @return JSON - document object
     */
    public function updateDocument( array $data = array() , array $query = array() )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::DOCUMENT;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'/'.$data['id'].'?'.$params;
        }

        $this->documents = $this->put( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Unsnap.
     *
     * @return Integer - count
     */ 
    public function unsnap( $documentId = NULL, $languageKey = 'en' )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::UNSNAP);
        $api = str_replace("<languageKey>", $languageKey, $api);

        $this->documents = $this->delete( $api, $header );

        return $this->documents;
    }

    /**
     * Mark document language version as reviewed.
     *
     * @return Integer - count
     */ 
    public function markDocLangVerReviewed( $documentId = NULL, $documentLangVerId = NULL ) 
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::VERREVIEW);
        $api = str_replace("<langVersionId>", $documentLangVerId, $api);

        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Decline review of language-version.
     *
     * @return Integer - count
     */
    public function declineDocLangVerReview( $documentId = NULL, $documentLangVerId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::VERREVIEWDECLINE);
        $api = str_replace("<langVersionId>", $documentLangVerId, $api);

        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Set additional properties.
     *
     * @return JSON - document object/Error Object
     */
    public function setAdditionalProperties( array $data = array() , $documentId = NULL )
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::ADDITIONALPROP);
        
        $this->documents = $this->put( $api, $data, $header );

        return $this->documents;
    }

    /**
     * Is following (logged-in account follows a document).
     *
     * @return Boolean - true / false
     */
    public function isFollowing( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::FOLLOW);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Follow document.
     *
     * @return Integer - count
     */
    public function follow( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::FOLLOW);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Follow document (other account).
     *
     * @return Integer - count
     */
    public function followByOtherAccount( $documentId = NULL, $accountId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::FOLLOWBYACCOUNT);
        $api = str_replace("<accountId>", $accountId, \Matterial\Matterial\ApiConstants::FOLLOWBYACCOUNT);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Unfollow document.
     *
     * @return Integer - count
     */
    public function unfollow( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::UNFOLLOW);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Unfollow document (other account).
     *
     * @return Integer - count
     */
    public function unfollowByOtherAccount( $documentId = NULL, $accountId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::UNFOLLOWBYACCOUNT);
        $api = str_replace("<accountId>", $accountId, \Matterial\Matterial\ApiConstants::UNFOLLOWBYACCOUNT);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Archive document.
     *
     * @return JSON - document Object/Error Object
     */
    public function archive( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::ARCHIVE);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Unarchive document.
     *
     * @return JSON - document Object/Error Object
     */
    public function unarchive( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::UNARCHIVE);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }
        
        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Trash document.
     *
     * @return JSON - document Object/Error Object
     */
    public function trash( $documentId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::TRASH);

        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Untrash document.
     *
     * @return JSON - document Object/Error Object
     */
    public function untrash( $documentId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::UNTRASH);

        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }


    /**
     * Remove document.
     *
     * @return JSON - document Object/Error Object
     */
    public function remove( $documentId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::DOCUMENT."/".$documentId;

        $this->documents = $this->delete( $api, $header );

        return $this->documents;
    }

    /**
     * Remove a specific version.
     *
     * @return JSON - document Object/Error Object
     */
    public function removeVersion( $documentId = NULL, $langVersionId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::REMOVEVERSION);
        $api = str_replace("<langVersionId>", $langVersionId, $api);

        $this->documents = $this->delete( $api, $header );

        return $this->documents;
    }

    /**
     * Remove a specific language.
     *
     * @return JSON - document Object/Error Object
     */
    public function removeLang( $documentId = NULL, $languageKey = NULL)
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::REMOVELANG);
        $api = str_replace("<languageKey>", $languageKey, $api);

        $this->documents = $this->delete( $api, $header );

        return $this->documents;
    }

    /**
     * Cleanup Trash.
     *
     * @return Integer - count of documents placed into removal-queue
     */
    public function cleanupTrash( array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::DOCUMENT;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->delete( $api, $header );

        return $this->documents;
    }

    /**
     * Get removal queue size.
     *
     * @return Integer - queue size
     */
    public function getRemovalQueueSize()
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->get( \Matterial\Matterial\ApiConstants::REMOVALQUEUE, $header );

        return $this->documents;
    }

    /**
     * Reconvert document-language-version.
     *
     * @return Integer - count of document-language-versions
     */
    public function reconvertDocLangVer( $documentId = NULL, $langVersionId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::RECONVERTLANGVER);
        $api = str_replace("<langVersionId>", $langVersionId, $api);

        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Convert all document-language-versions.
     *
     * @return Integer - count of document-language-versions
     */
    public function convert( array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::CONVERT;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->put( $api, NULL, $header );

        return $this->documents;
    }

    /**
     * Get main-file by document-id.
     *
     * @return File as stream
     */
    public function getMainFileByDocId( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::GETMAINFILE);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Get main-file by document-id and document-language-version-id.
     *
     * @return File as stream
     */
    public function getMainFileByDocLangVerId( $documentId = NULL, $langVersionId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::GETMAINFILEBYVER);
        $api = str_replace("<langVersionId>", $langVersionId, $api);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Compare main-files by document-language-version-ids.
     *
     * @return File as stream
     */
    public function compareMainFileByDocLangVerId( $oldDocumentLanguageVersionId = NULL , $newDocumentLanguageVersionId = NULL )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<oldDocLangVerId>", $oldDocumentLanguageVersionId, \Matterial\Matterial\ApiConstants::COMPMAINFILES);
        $api = str_replace("<newDocLangVerId>", $newDocumentLanguageVersionId, $api);

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Get attachments.
     *
     * @return JSON
     */
    public function getAttachments( $documentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::ATTACHMENT);

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Get attachment by document-id, document-language-version-id, attachment-id.
     *
     * @return JSON
     */
    public function getAttachmentsByOptions( $documentId = NULL, $langVersionId = NULL, $attachmentId = NULL, array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        if($langVersionId != NULL) 
        {
            $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::ATTACHBYVERLANG);
            $api = str_replace("<langVersionId>", $langVersionId, $api);
            $api = str_replace("<attachmentId>", $attachmentId, $api);
        } else {
            $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::ATTACHBYATTID);
            $api = str_replace("<attachmentId>", $attachmentId, $api);
        }
        

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Execute batch action on clipboard.
     *
     * @return Boolean - always true, because this executes the batch action asynchronously
     */
    public function exeBatchActionClipboard( $actionCode = 0 )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->put( \Matterial\Matterial\ApiConstants::EXCECUTEBATCHONCP, NULL, $header );

        return $this->documents;
    }

    /**
     * Search Document on the basis of query parameter
     *
     * @return JSON list of documents found
     */
    public function search( array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::SEARCH;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Clears the document-part of the index and reindexes all documents for the current instance.
     *
     * @return NULL
     */
    public function reIndexAll()
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->post( \Matterial\Matterial\ApiConstants::REINDEXALL, NULL, $header );

        return $this->documents;
    }

    /**
     * Search autocomplete.
     *
     * @return JSON list of search autocomplete suggests
     */
    public function searchAutoComplete( array $query = array() )
    {
        $header = array(
            'content-type' => 0,
            'accept' => 1,
            'show' => 0
        );

        $api = \Matterial\Matterial\ApiConstants::SEARCHAUTOCOMPLETE;

        if(count($query) > 0) {
            $params = http_build_query($query);
            
            $api = $api.'?'.$params;
        }

        $this->documents = $this->get( $api, $header );

        return $this->documents;
    }

    /**
     * Reindexes one document by id.
     *
     * @return NULL
     */
    public function index()
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $this->documents = $this->post( \Matterial\Matterial\ApiConstants::INDEX, NULL, $header );

        return $this->documents;
    }

    /**
     * Reindexes one document by language-version-id.
     *
     * @return NULL
     */
    public function indexDocLangVersion()
    {
        $header = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 0
        );

        $api = str_replace("<documentId>", $documentId, \Matterial\Matterial\ApiConstants::INDEXLANGVERSION);
        $api = str_replace("<langVersionId>", $langVersionId, $api);

        $this->documents = $this->post( $api, NULL, $header );

        return $this->documents;
    }

}