<?php

namespace Matterial\Matterial\Api;

/**
 * Document Api Wrapper
 *
 * @link   https://www.matterial.com/en/documentation/api/311#document
 * @author Matterial.com
 */
class Authenticate extends AbstractApi
{
    protected $response;
    /**
     * Checks email and password. 
     *
     * @return JSON Gets all available datasources for current user.
     */
    public function preLogin(array $data = NULL)
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $this->response = $this->post('api/logon/prelogin',$data, $header);

        return $this->response;
    }

    /**
     * Checks JSESSIONID. 
     *
     * @return JSON Gets all available datasources for current user.
     */
    public function preChangeInstance()
    {
        $header = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 0
        );

        $this->response = $this->put('api/logon/prechangeinstance', NULL, $header);

        return $this->response;
    }

    /**
     * Checks username and password only 
     *
     * @return JSON Gets all available datasources for current user.
     */
    public function check(array $data = NULL)
    {
        $header = array(
            'content-type' => 1,
            'accept' => 0,
            'show' => 0
        );
        
        $this->response = $this->post('api/logon/check', NULL, $header);

        return $this->response;
    }
}