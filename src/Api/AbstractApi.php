<?php

namespace Matterial\Matterial\Api;

use Matterial\Matterial\Client;

/**
 * Abstract class for Api classes
 *
 * @author Matterial.com
 */
abstract class AbstractApi
{
    /**
     * The client
     *
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritDoc}
     */
    protected function get($path, $header)
    {
        return $this->client->get($path, $header);
    }

    /**
     * {@inheritDoc}
     */
    public function post($path, $data, $header)
    {
        return $this->client->post($path, $data, $header);
    }

    /**
     * {@inheritDoc}
     */
    protected function put($path, $data, $header)
    {
        return $this->client->put($path, $data, $header);
    }

    /**
     * {@inheritDoc}
     */
    protected function delete($path, $header)
    {
        return $this->client->delete($path, $header);
    }   
}