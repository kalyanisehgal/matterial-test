<?php
/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Matterial\Matterial;

class Constants
{
    /**
     * General Constants
     */
    public const JSESSIONID  = 'JSESSIONID';
    public const LOCATION    = 'Location';
    
    /**
     * API login types constants
     */
    public const PRELOGON            = "prelogin";
    public const LOGON               = "login";
    public const LOGON_NTML          = "ntml";
    public const LOGON_AUTH2_GOOGLE  = "google";
    public const LOGON_AUTH2_COBOT   = "cobot";
    public const LOGON_AUTH2_MS      = "microsoft";
    public const LOGON_LTI           = "lti";
    public const LOGON_SAML          = "saml";
    public const LOGON_AUTOLOGIN     = "autologin";
    public const LOGON_PASSWORDRESET = "passreset";

    /**
     * API Classes contants
     */
    public const AUTHENTICATE = "Authenticate";
    public const DOCUMENT = "Document";
}