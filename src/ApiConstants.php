<?php
/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Matterial\Matterial;

class ApiConstants
{
    /**
     * LOGIN API Contants
     */
    public const LOGIN               = 'api/logon';
    public const PRELOGIN            = 'api/logon/prelogin';
    public const LOGINSERVERREDIRECT = 'api/logon/login';
    public const PRECHAGEINSTANCE    = 'api/logon/prechangeinstance';

    /**
     * DOCUMENT API Constants
     */
    public const DOCUMENT           = 'api/document';
    public const DUPLICATEBYID      = 'api/document/<documentId>/duplicate';
    public const DUPLICATEBYCONTEXT = 'api/document/<contextToken>/duplicate';
    public const DUPLICATES         = 'api/document/duplicate';
    public const GETTRASHDOCUMENTS  = 'api/document/trash';
    public const ADDCBDOCUMENTS     = 'api/document/clipboard/add';
    public const REMOVECBDOCUMENTS  = 'api/document/clipboard/remove';
    public const CLEARCBDOCUMENTS   = 'api/document/clipboard';
    public const CREATESNAP         = 'api/document/snap';
    public const TEMPLATE           = 'api/document/template';
    public const UPDATEBIO          = 'api/document/bio';
    public const UNSNAP             = 'api/document/<documentId>/language/<languageKey>/snap';
    public const VERREVIEW          = 'api/document/<documentId>/version/<langVersionId>/review';
    public const VERREVIEWDECLINE   = 'api/document/<documentId>/version/<langVersionId>/reviewdecline';
    public const ADDITIONALPROP     = 'api/document/<documentId>/additionalproperty';
    public const FOLLOW             = 'api/document/<documentId>/follow';
    public const FOLLOWBYACCOUNT    = 'api/document/<documentId>/follow/byaccount/<accountId>';
    public const UNFOLLOW           = 'api/document/<documentId>/unfollow';
    public const UNFOLLOWBYACCOUNT  = 'api/document/<documentId>/unfollow/byaccount/<accountId>';
    public const ARCHIVE            = 'api/document/<documentId>/archive';
    public const UNARCHIVE          = 'api/document/<documentId>/unarchive';
    public const TRASH              = 'api/document/<documentId>/trash';
    public const UNTRASH            = 'api/document/<documentId>/untrash';
    public const REMOVEVERSION      = 'api/document/<documentId>/version/<langVersionId>';
    public const REMOVELANG         = 'api/document/<documentId>/language/<languageKey>';
    public const REMOVALQUEUE       = 'api/document/removal/queue';
    public const RECONVERTLANGVER   = 'api/document/<documentId>/version/<langVersionId>/convert';
    public const CONVERT            = 'api/document/convert';
    public const GETMAINFILE        = 'api/document/<documentId>/file';
    public const GETMAINFILEBYVER   = 'api/document/<documentId>/version/<langVersionId>/file';
    public const COMPMAINFILES      = 'api/document/compare/version/<oldDocLangVerId>/<newDocLangVerId>/file';
    public const ATTACHMENT         = 'api/document/<documentId>/attachment';
    public const ATTACHBYVERLANG    = 'api/document/<documentId>/version/<langVersionId>/attachment/<attachmentId>';
    public const ATTACHBYATTID      = 'api/document/<documentId>/attachment/<attachmentId>';
    public const EXCECUTEBATCHONCP  = 'api/document/batchaction/<actionCode>';
    public const SEARCH             = 'api/document/search';
    public const REINDEXALL         = 'api/document/search/reindexall';
    public const SEARCHAUTOCOMPLETE = 'api/document/search/autocomplete';
    public const INDEX              = 'api/document/search/index';
    public const INDEXLANGVERSION   = 'api/document/search/index/<documentId>/version/<langVersionId>';
    
}