<?php
/*
 * Copyright 2020 Matterical.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);
namespace Matterial\Matterial;

use Psr\Log\LoggerInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler as MonologStreamHandler;

class Client
{
    /**
     * Define API End Point
     */
    private $API_END_POINT = "https://my.matterial.com/mtr-backend/";
    
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var boolean
     */
    private $checkSslCertificate = false;

    /**
     * @var boolean
     */
    private $checkSslHost = false;

    /**
     * @var int|null Matterial response code, null if request is not still completed
     */
    private $responseCode = null;

    /**
     * @var Psr\Log\LoggerInterface $logger
     */
    private $logger;

    /**
     * @var Cookie JSESSIONID
     */
    private $JSESSIONID;

    /**
     * Error strings if json is invalid
     */
    private static $json_errors = array(
        JSON_ERROR_NONE      => 'No error has occurred',
        JSON_ERROR_DEPTH     => 'The maximum stack depth has been exceeded',
        JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
        JSON_ERROR_SYNTAX    => 'Syntax error',
    );

    /**
     * CURL response codes
     */
    private static $curl_codes = array(
       200 => 'OK. The request has succeeded.',
       202 => 'Accepted. The request has been accepted for processing.',
       400 => 'Bad Request. The request could not be understood by the server due to malformed syntax.',
       401 => 'Unauthorized. The request requires user authentication.',
       403 => 'Forbidden. The server understood the request, but is refusing to fulfill it.',
       404 => 'Not Found. The server has not found anything matching the Request-URI.',
       406 => 'Not Acceptable. The resource identified by the request is only capable of generating response entities which have content characteristics not acceptable according to the accept headers sent in the request.',
       500 => 'Internal Server Error. The server encountered an unexpected condition which prevented it from fulfilling the request.'
    );

    /**
     * Construct the Matterial Client.
     *
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        $this->config = array_merge(
            [
              // Don't change these unless you're working against a special development
              // or testing environment.
              'end_point' => $this->API_END_POINT,
    
              // Credentials to generate JSESSIONID 
              'login' => '',
              'password' => '',

              //Path to save logs
              'log_path' => '',
            ],
            $config
        );
    }

    /**
     * @param  string  $name
     * @return Api\AbstractApi
     * @throws \InvalidArgumentException
     */
    public function api($name)
    {
        if (!isset($name)) {
            throw new \InvalidArgumentException();
        }
        if (isset($this->apis[$name])) {
            return $this->apis[$name];
        }
        $class = 'Matterial\Matterial\Api\\'.$name;
        $this->apis[$name] = new $class($this);
        
        return $this->apis[$name];
    }

    /**
     * HTTP GETs a json $path and tries to decode it
     * @param  string $path
     * @return array
     */
    public function get($path, $header)
    {
        if (false === $json = $this->curlRequest($path, 'GET', null, $header)) {
            return false;
        }

        return $this->decode($json);
    }

    /**
     * Decodes json response
     * @param  string $json
     * @return array
     */
    public function decode($json)
    {
        $decoded = json_decode($json, true);
        if (null !== $decoded) {
            return $decoded;
        }
        if (JSON_ERROR_NONE === json_last_error()) {
            return $json;
        }

        return self::$json_errors[json_last_error()];
    }

    /**
     * HTTP POSTs $params to $path
     * @param  string $path
     * @param  string $data
     * @return mixed
     */
    public function post($path, $data, $header)
    {
        if (false === $json = $this->curlRequest($path, 'POST', $data, $header)) {
            return false;
        }

        return $this->decode($json);
    }

    /**
     * HTTP PUTs $params to $path
     * @param  string $path
     * @param  string $data
     * @return array
     */
    public function put($path, $data, $header)
    {
        if (false === $json = $this->curlRequest($path, 'PUT', $data, $header)) {
            return false;
        }

        return $this->decode($json);
    }

    /**
     * HTTP DELETEs $params to $path
     * @param  string $path
     * @return array
     */
    public function delete($path, $header)
    {
        if (false === $json = $this->curlRequest($path, 'DELETE', null, $header)) {
            return false;
        }

        return $this->decode($json);
    }

    /**
     * Turns on/off ssl certificate check
     * @param  boolean $check
     * @return Client
     */
    public function setCheckSslCertificate($check = true)
    {
        $this->checkSslCertificate = $check;

        return $this;
    }

    /**
     * Turns on/off ssl host certificate check
     * @param  boolean $check
     * @return Client
     */
    public function setCheckSslHost($check = true)
    {
        $this->checkSslHost = $check;

        return $this;
    }

    /**
     * Set the Logger path
     * @param string
     */
    public function setLoggerPath($path)
    {
        $this->config['log_path'] = $path;
    }

    /**
     * Set the Logger object
     * @param Psr\Log\LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return Psr\Log\LoggerInterface implementation
     */
    public function getLogger()
    {
        if (!isset($this->logger)) {
        $this->logger = $this->createDefaultLogger();
        }

        return $this->logger;
    }

    protected function createDefaultLogger()
    {
        $logger = new Logger('matterial-api-client');
        
        $handler = new MonologStreamHandler($this->config['log_path'], Logger::DEBUG);
        
        $logger->pushHandler($handler);

        return $logger;
    }

    /**
     * Returns Matterial response code
     * @return int
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * @return JSESSIONID
     */
    public function isLoggedIn() 
    {
        if(isset($this->JSESSIONID)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Extract JSESSIONID from CURL repsonse header
     * @return JSESSIONID
     */
    private function getVarFromHeader($header, $var) 
    {
        if($var == 'JSESSIONID') {
            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
            $cookies = array();
            foreach($matches[1] as $item) {
                parse_str($item, $cookie);
                $cookies = array_merge($cookies, $cookie);
            }

            if(isset($cookies['JSESSIONID'])) {
                return $cookies['JSESSIONID'];
            }
        }

        if($var == 'Location') {
            preg_match_all('/^Location:\s*([^;]*)/mi', $header, $matches);
            
            if(isset($matches[1][0])){
                $location =  explode('api',$matches[1][0]);
                if(isset($location[0])){
                    return $location[0];
                }
            }
        }
        
        return false;
    }
    

    /**
     * Creates session
     * @return TRUE
     */
    public function createSession($type) 
    {
        switch ($type) {
            case Constants::LOGON: return $this->login(); break;
            case Constants::PRELOGON: return $this->preLogin();break;        
        } 
    }

    /**
     * Checks email and password and sets JSESSIONID. 
     *
     * @return API End point.
     */
    private function preLogin() 
    {
        $data = array (
            'login' => $this->config['login'],
            'password' => $this->config['password']
        );

        $headerSettings = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 1
        );

        $response = $this->curlRequest(ApiConstants::PRELOGIN, 'POST', $data, $headerSettings);

        $this->JSESSIONID = $this->getVarFromHeader($response[0], Constants::JSESSIONID);

        $data = $this->decode($response[1]);

        //Get the instance ID
        if(count($data) != 0) {
            $instanceID = $data[0]['id'];
        }
        
        $headerSettings = array(
            'content-type' => 0,
            'accept' => 0,
            'show' => 1
        );

        if(isset($instanceID)) {
            $response = $this->curlRequest(ApiConstants::LOGINSERVERREDIRECT.'/'.$instanceID, 'GET', NULL, $headerSettings);
 
            $this->API_END_POINT = $this->getVarFromHeader($response[0], Constants::LOCATION);
        }

        return $this->API_END_POINT;
    }

    /**
     * Checks email and password and sets JSESSIONID. 
     *
     * @return TRUE
     */
    private function login() 
    {
        $data = array (
            'login' => $this->config['login'],
            'password' => $this->config['password']
        );

        $headerSettings = array(
            'content-type' => 1,
            'accept' => 1,
            'show' => 1
        );

        $response = $this->curlRequest(ApiConstants::LOGIN, 'POST', $data, $headerSettings);

        $this->JSESSIONID = $this->getVarFromHeader($response[0], Constants::JSESSIONID);
        
        return true;
    }

    /**
     * @param  string $path
     * @param  string $method
     * @param  array  $data
     * @return false|array|string
     * @throws \Exception If anything goes wrong on curl request
     */
    private function curlRequest($path, $method = 'GET',array $data = null, array $header)
    {
        //Initialize url
        $curl = curl_init();

        //Set the curl end point
        curl_setopt($curl, CURLOPT_URL, $this->API_END_POINT.$path);
        
        //General curl option settings
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURLOPT_HTTP_VERSION);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->checkSslCertificate);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->checkSslHost);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //Headers
        $httpHeader = array();

        //Check if content type header settings
        if($header['content-type'] == 1)
        $httpHeader[] = 'Content-Type: application/json';

        //Check the content accept header settings
        if($header['accept'] == 1)
        $httpHeader[] = 'Accept: application/json';
        
        //Set the session cookie in the header (if session is active)
        if(isset($this->JSESSIONID)){
            $httpHeader[] = 'Cookie: JSESSIONID='.$this->JSESSIONID;
        }

        //Set the header
        curl_setopt($curl, CURLOPT_HTTPHEADER, $httpHeader);

        //Check show header in response settings
        if($header['show'] == 1){
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }
        
        //Set curl method
        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
                if (isset($data)) {curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));}
                break;
            case 'PUT':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                if (isset($data)) {curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));}
                break;
            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default: // GET
                break;
        }

        //Recieve reponse of curl request
        $response = curl_exec($curl);

        //Store the response code
        echo $this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        echo "<br/>";

        //Throw exception in case response code other than success
        if(!in_array($this->responseCode, array(200,303))) {
            throw new MattException($this->responseCode." ".self::$curl_codes[$this->responseCode]);
        }

        //Extract header form response
        if($header['show'] == 1){
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $responseHeader[0] = substr($response, 0, $header_size);
            $responseHeader[1] = substr($response, $header_size);
        }

        //Throw curl error if any
        if (curl_errno($curl)) {
            $e = new \Exception(curl_error($curl), curl_errno($curl));
            curl_close($curl);
            throw $e;
        }

        //Close the curl request
        curl_close($curl);

        //Return final response of curl request
        if($header['show'] == 1){
            return $responseHeader;
        } else {
            if ($response) {  
                return $response;
            }
        } 

        return true;
    }

}
